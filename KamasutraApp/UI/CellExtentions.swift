//
//  CellExtentions.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 1/26/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit
import AlamofireImage

typealias OptionalStringHandler = (String?) -> ()
typealias StringHandler = (String) -> ()

enum SectionType : Int, CustomStringConvertible {
    
    case catalogType = 0
    case categoriesType = 1
    case listType = 2
    case gamesType = 3
    case myRewardsType = 4
    
    var description: String {
        switch self {
        case .catalogType:
            return "CATALOG"
        case .categoriesType:
            return "CATEGORIES"
        case .listType:
            return "LISTS"
        case .gamesType:
            return "GAMES"
        case .myRewardsType:
            return "MY REWARDS"
        }
    }
}

protocol Rootable {
    var rootNavigationViewController : UINavigationController? { get }
}

protocol ImageDownloadable {
    func downloadImageInto(imageView : UIImageView)
}

protocol EndImageDownloadable {
    func endDownloadImageInto(imageView : UIImageView)
}

extension EndImageDownloadable {
    func endDownloadImageInto(imageView : UIImageView) {
        imageView.af_cancelImageRequest()
    }
}

protocol Titlable {
    func titleWith(textHandler : OptionalStringHandler?)
}

protocol Descriptionable {
    func descriptionWith(textHandler : OptionalStringHandler?)
}

protocol Shadowable : class {
    
    var containerView : UIView! { get set } //may be @IBOutlet
    func configureShadow()
}

protocol Shakable : class {
    func shake()
}

protocol Categorical {
    var sectionTitle : String? { get }
}

protocol Multiplyerable {
    var multiplyer : CGFloat { get }
}

protocol CellIdentifiable {
    var cellIdentifier : String? { get }
}

protocol SectionTypable {
    var sectionType: SectionType { get set}
}

protocol Settingsable {
    var notShowTimeLabel : Bool { get }
    var settingsTitle : String { get }
}

// MARK : - Extensions

extension Shadowable {
    func configureShadow() {
        let shadowPath = UIBezierPath(rect: containerView.bounds)
        containerView.layer.masksToBounds = false
        containerView.layer.shadowColor = UIColor.smoothRose.cgColor
        containerView.layer.shadowOffset = CGSize(width: -1.0, height: 2.0)
        containerView.layer.shadowOpacity = 0.3
        containerView.layer.shadowRadius = 5
        containerView.layer.shadowPath = shadowPath.cgPath
    }
}

extension Shakable where Self : UIView {
    
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "position.x")
        animation.values = [0.0, 0.0, 0.0, -10.0, 10.0, 0.0]
        animation.keyTimes = [NSNumber(value: 0.0),
                              NSNumber(value: 1.0 / 6.0),
                              NSNumber(value: 3.0 / 6.0) ,
                              NSNumber(value: 5.0 / 6.0),
                              NSNumber(value: 1.0)]
        animation.duration = 0.4
        animation.isAdditive = true
        layer.add(animation, forKey: "shake")
    }
}

