//
//  Extensions.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 1/25/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

protocol Transparentable {
    func transparentBar()
}

extension Transparentable where Self : UINavigationController {
    func transparentBar() {
        navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
    }
}

extension UIView {
    class func instanceFromNib<T : UIView>() -> T? {
        return UINib(nibName: String(describing: T.self), bundle: nil).instantiate(withOwner: nil, options: nil).first as? T
    }
}

extension UIView {
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
}

extension UIView {
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}

extension UIColor {
    static let smoothRose = UIColor(red: 209.0 / 255.0,
                                    green: 178.0 / 255.0,
                                    blue: 225.0 / 255.0,
                                    alpha: 1.0)
    static let darkRose = UIColor(red: 106.0 / 255.0,
                                  green: 85.0 / 255.0,
                                  blue: 97.0 / 255.0,
                                  alpha: 0.7)
}

extension UITabBarItem {
    
    convenience init(title: String?, tag : Int) {
        self.init(title: title, image: nil, tag: tag)
    }
}

extension UIViewController {
    func configureBurgerItem() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back_arrow"), style: .plain, target: self, action: #selector(burgerItemTapped(_:)))
    }
    
    @objc func burgerItemTapped(_ sender: UIBarButtonItem) {
        presentLeftMenuViewController()
    }
}

extension String {
    
    var isEmail: Bool {
        let parts = components(separatedBy: "@")
        if parts.count == 2, parts[1].components(separatedBy: ".").count > 1 { return true }
        return false
    }
}

