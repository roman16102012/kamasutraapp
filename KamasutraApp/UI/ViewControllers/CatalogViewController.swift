//
//  CatalogViewController.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/8/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

final class CatalogViewController: UIViewController {
    
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var menuContainerView: UIView!
    
    lazy var viewModel : ItemViewModel = {
        return ItemViewModel()
    }()
    
    // MARK : - Life Cycle of UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureComponents()
        configureMenu()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Configuration methods
    
    private func configureComponents() {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.register(UINib(nibName: String(describing: ItemTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ItemTableViewCell.self))
    }
    
    private func configureMenu() {}
}

// MARK: - UITableViewDataSource & UITableViewDelegate

extension CatalogViewController:  UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ItemTableViewCell.self), for: indexPath) as? ItemTableViewCell
        return cell ?? ItemTableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let displayCell = cell as? ItemTableViewCell
        displayCell?.configure(delegate: viewModel)
        displayCell?.tappButtonItemHandler = {(index) in
            print(index)
        }
        displayCell?.tapHandler = {[weak applicationController = iOS.shared.applicationController](cell) in
            applicationController?.presentCategoryOn()
        }
    }
}
