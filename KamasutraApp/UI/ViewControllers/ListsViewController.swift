//
//  ListsViewController.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 1/25/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

final class ListsViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    
    private lazy var viewModel : ItemViewModel = {
        return ItemViewModel()
    }()
    // MARK : - Life Cycle UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureComponents()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Configuration methods
    private func configureComponents() {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.register(UINib(nibName: String(describing: ItemTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ItemTableViewCell.self))
    }
}

extension ListsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ItemTableViewCell.self), for: indexPath) as? ItemTableViewCell
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let displayCell = cell as? ItemTableViewCell
        displayCell?.configure(delegate: viewModel)
        displayCell?.tappButtonItemHandler = {(index) in
            print(index)
        }
    }
}
