//
//  BaseSignViewController.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/12/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

enum SignType : Int {
    case signIn
    case signUp
}

final class BaseSignViewController: UIViewController {
    
    @IBOutlet private weak var signUpView: UIView!
    @IBOutlet private weak var signInView: UIView!
    @IBOutlet private weak var segmentControl: UISegmentedControl!
    
    @IBOutlet private weak var containerView: UIView!
    private weak var segmentItem : UIView?
    
    // MARK : - Life Cycle of UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureContainerView()
        configureSegmentControl()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK : - Configuration methods
    
    private func configureSegmentControl() {
        segmentControl.tintColor = UIColor.clear
        segmentControl.setTitleTextAttributes([NSAttributedStringKey.foregroundColor : UIColor.black,
                                               NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Bold", size: 16)!],
                                              for: .selected)
        segmentControl.setTitleTextAttributes([NSAttributedStringKey.foregroundColor : UIColor.black,
                                               NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Light", size: 16)!],
                                              for: .normal)
        segmentControlChanged(segmentControl)
    }
    
    private func configureSegmentItemsWith(index : Int) {
        segmentItem?.removeFromSuperview()
        let item = segmentControl.subviews[index] as UIView
        let backgroundView = UIView(frame: item.bounds)
        let roundRect = backgroundView.bounds
        let roundedView = UIView(frame: .zero)
        backgroundView.addSubview(roundedView)
        roundedView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        roundedView.frame = roundRect
        roundedView.backgroundColor = UIColor.darkRose
        roundedView.cornerRadius = 10
        item.addSubview(backgroundView)
        segmentItem = backgroundView
        backgroundView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    private func configureContainerView() {
        containerView.backgroundColor = UIColor.darkRose
    }
    
    // MARK : - Handlers
    
    @IBAction private func segmentControlChanged(_ sender: UISegmentedControl) {
        guard let signType = SignType(rawValue: sender.selectedSegmentIndex) else { return }
        let signUpAlfa : CGFloat!
        let signInAlfa : CGFloat!
        switch signType {
        case .signIn:
            signUpAlfa = 1.0
            signInAlfa = 0.0
        case .signUp:
            signUpAlfa = 0.0
            signInAlfa = 1.0
        }
        UIView.animate(withDuration: 0.5) {[weak self] in
            self?.signInView.alpha = signInAlfa
            self?.signUpView.alpha = signUpAlfa
        }
        configureSegmentItemsWith(index: 1 - signType.rawValue)
    }
}
