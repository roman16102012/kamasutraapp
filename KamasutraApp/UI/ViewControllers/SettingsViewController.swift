//
//  SettingsViewController.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/21/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

final class SettingsViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    
    // MARK : - Life Cycle of UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK : - Methods of configuration
    
    private func configureTableView() {
        tableView.estimatedRowHeight = navigationController?.navigationBar.frame.size.height ?? 0
        tableView.rowHeight = UITableViewAutomaticDimension
    }
}

// MARK : - UITableViewDelegate & UITableViewDataSource

extension SettingsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = SettingsViewModel(index: indexPath.row) else { return BaseSettingsTableViewCell() }
        let cell = tableView.dequeueReusableCell(withIdentifier: viewModel.cellIdentifier ?? "", for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let viewModel = SettingsViewModel(index: indexPath.row) else { return }
        let displayCell = cell as? BaseSettingsTableViewCell
        displayCell?.configure(delegate: viewModel)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        iOS.shared.applicationController.presentChangePassword()
    }
}
