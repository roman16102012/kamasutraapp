//
//  ChangePasswordViewController.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/21/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

final class ChangePasswordViewController: UIViewController {

    @IBOutlet private weak var oldPasswordTextField: UITextField!
    @IBOutlet private weak var newPasswordTextField: UITextField!
    @IBOutlet private weak var confirmPasswordTextField: UITextField!
    
    // MARK: - UIViewController life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
