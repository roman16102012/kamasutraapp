//
//  ProfileViewController.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/9/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

final class ProfileViewController: UITableViewController {
    
    private var isEditedMode: Bool = false {
        didSet {
            print(isEditedMode)
        }
    }
    
    // MARK : - Life Cycle of UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK : - Configuration methods
    
    // MARK : - Handler
    
    @IBAction private func settingsButtonTapped(_ sender: UIBarButtonItem) {
        iOS.shared.applicationController.presentSettings()
    }
    
    @IBAction private func changeModeButtonTapped(_ sender: UIBarButtonItem) {
        isEditedMode = !isEditedMode
    }
}
