//
//  NavigationController.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 1/26/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

final class LoginNavigationController : UINavigationController, Transparentable {
    override func viewDidLoad() {
        super.viewDidLoad()
        transparentBar()
    }
}

final class HomeNavigationController: UINavigationController{
}

final class PartnersNavigationController: UINavigationController {
}

final class ProfileNavigationController: UINavigationController {
}

final class ListsNavigationController: UINavigationController {
}

final class RewardsNavigationController: UINavigationController {
}
