//
//  MenuViewController.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 1/24/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

final class MenuViewController: UIViewController {
    
    @IBOutlet weak private var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    // MARK : - Configuration methods
    
    private func configureView() {
        let view = TimeStickView.instanceFromNib() ?? TimeStickView()
        view.frame = containerView.bounds
        containerView.addSubview(view)
    }
}
