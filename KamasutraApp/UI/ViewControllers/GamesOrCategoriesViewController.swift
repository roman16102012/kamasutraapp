//
//  Games&CategoriesViewController.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/6/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

final class GamesOrCategoriesViewController: UIViewController {
    
    @IBOutlet weak private var collectionView: UICollectionView!
    
    private var detailsViewModel : DetailsViewModel?
    
    // MARK : - Life Cycle of UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationTitle()
        configureCollectionViewCell()
    }
    
    // MARK : - Configuration methods
    
    func configure<T : Categorical & Multiplyerable & CellIdentifiable>(delegate : T) {
        detailsViewModel = delegate as? DetailsViewModel
    }
    
    private func configureCollectionViewCell() {
        collectionView.register(UINib(nibName: detailsViewModel?.cellIdentifier ?? "", bundle: nil), forCellWithReuseIdentifier: detailsViewModel?.cellIdentifier ?? "")
    }
    
    private func configureNavigationTitle() {
        navigationItem.title = detailsViewModel?.sectionTitle
    }
}

// MARK : - UICollectionViewDelegate & UICollectionViewDataSource

extension GamesOrCategoriesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: detailsViewModel?.cellIdentifier ?? "", for: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width , height: collectionView.frame.height / (detailsViewModel?.multiplyer ?? 1.0))
    }
}
