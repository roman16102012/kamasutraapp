//
//  HomeViewController.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 1/26/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

final class HomeViewController: UITableViewController {
    
    // MARK : - UIViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureBurgerItem()
        configureTableView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK : - Configuration methods
    
    private func configureTableView() {
        tableView.estimatedRowHeight = navigationController?.navigationBar.frame.size.height ?? 0
        tableView.estimatedSectionHeaderHeight = navigationController?.navigationBar.frame.size.height ?? 0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        let catalogCellIdentifier = String(describing: CatalogTableViewCell.self)
        let headerCellIdentifier = String(describing: SelectCategoryHeaderView.self)
        tableView.register(UINib(nibName: catalogCellIdentifier, bundle: nil), forCellReuseIdentifier: catalogCellIdentifier)
        tableView.register(UINib(nibName: headerCellIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: headerCellIdentifier)
    }
}

// MARK : - TableViewDataSource / TableViewDelegate

extension HomeViewController {
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CatalogTableViewCell.self), for: indexPath) as? CatalogTableViewCell
        return cell ?? CatalogTableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let sectionViewModel = SectionViewModel(index: indexPath.section) else { return }
        let displayCell = cell as? CatalogTableViewCell
        displayCell?.configure(delegate: sectionViewModel)
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: String(describing: SelectCategoryHeaderView.self)) as? SelectCategoryHeaderView
        return headerView ?? SelectCategoryHeaderView()
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let sectionViewModel = SectionViewModel(index: section) else {return}
        let displayHeaderView = view as? SelectCategoryHeaderView
        displayHeaderView?.configure(delegate: sectionViewModel)
        displayHeaderView?.sectionHandler = {[weak applicationController = iOS.shared.applicationController](sectionType) in
            guard let sectionType = sectionType else {return}
            applicationController?.presentHomeScreenDetails(sectionType: sectionType)
        }
    }
}
