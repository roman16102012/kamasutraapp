//
//  TabBarController.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 1/25/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

final class TabBarController: UITabBarController, UITabBarControllerDelegate, Rootable {
    
    var currentController : UIViewController?
    
    var rootNavigationViewController : UINavigationController? {
        return currentController as? UINavigationController
    }
    
    // MARK: - UIViewController Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        delegate = self
        currentController = viewControllers?.first
    }
    
    // MARK : - UITabBarControllerDelegate
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        currentController = viewController
    }
}


