//
//  SignInViewController.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/13/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

final class SignInViewController: UIViewController {
    
    @IBOutlet weak private var emailTextFiled: UITextField!
    @IBOutlet weak private var passwordTextFiled: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
