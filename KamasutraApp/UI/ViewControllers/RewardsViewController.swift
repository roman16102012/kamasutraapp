//
//  RewardsViewController.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/22/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

final class RewardsViewController: UIViewController {

    @IBOutlet private weak var collectionView: UICollectionView!

    // MARK : - Life Cycle of UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureBurgerItem()
        configureCollectionView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK : - Configuration methods
    
    private func configureCollectionView() {
        collectionView.register(UINib(nibName: String(describing: MyRewardCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: MyRewardCollectionViewCell.self))
    }
}

extension RewardsViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: MyRewardCollectionViewCell.self), for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2 - 10, height: collectionView.frame.height / 3)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
}
