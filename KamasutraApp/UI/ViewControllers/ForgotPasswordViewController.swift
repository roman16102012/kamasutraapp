//
//  ForgotPasswordViewController.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/21/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

final class ForgotPasswordViewController: UIViewController {
    
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var titleView: UIView!
    
    // MARK : - Life Cycle of UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureContainerView()
        configureTitleView()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK : - Configuration methods
    
    private func configureContainerView() {
        containerView.backgroundColor = UIColor.darkRose
    }
    
    private func configureTitleView() {
        titleView.backgroundColor = UIColor.darkRose
    }
}
