//
//  HomeDetailsViewController.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 1/29/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit
import UPCarouselFlowLayout

final class CategoryViewController: UIViewController  {
    
    @IBOutlet private weak var carusel: UICollectionView!
    @IBOutlet private weak var containerView: UIView!
    
    private lazy var centeredGapValue : CGFloat = {
        return carusel.frame.width / 9
    }()
    
    // MARK : - Life Cycle UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionViewCell()
        configureCollectionView()
        configureMenu()
    }
    
    // MARK : - Configuration methods
    
    private func configureCollectionView() {
        let layout = carusel.collectionViewLayout as? CenterCellCollectionViewFlowLayout
        layout?.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing: 0)
    }
    
    private func configureCollectionViewCell() {
        carusel.register(UINib(nibName: String(describing: CatalogCollectionItemView.self), bundle: nil), forCellWithReuseIdentifier: String(describing: CatalogCollectionItemView.self))
    }
    
    private func configureMenu() {}
}

// MARK : - UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout

extension CategoryViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: CatalogCollectionItemView.self), for: indexPath) as? CatalogCollectionItemView
        return cell ?? CatalogCollectionItemView()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 1.3, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: centeredGapValue, bottom: 0, right: centeredGapValue)
    }
}
