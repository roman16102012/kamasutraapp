//
//  PartnersViewController.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/9/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

final class PartnersViewController: UIViewController {

    @IBOutlet weak private var tableView: UITableView!
    
    // MARK : - Life Cycle of UIViewConrtoller

    override func viewDidLoad() {
        super.viewDidLoad()
        configureBurgerItem()
        configureComponents()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK : - Configuration methods
    
    private func configureComponents() {
        tableView.rowHeight = UITableViewAutomaticDimension
    }
}

extension PartnersViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PartnerTableViewCell.self), for: indexPath) as? PartnerTableViewCell
        return cell ?? PartnerTableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
}
