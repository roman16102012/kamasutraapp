//
//  ItemTableViewCell.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 1/25/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit
import Material

final class ItemTableViewCell: TableViewCell, Shadowable {

    typealias ItemButtomHandler = (Int) -> ()
    typealias TapHandler = (ItemTableViewCell) -> ()
    
    @IBOutlet weak private var iconImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak private var nameTitleLabel: UILabel!
    @IBOutlet weak private var descriptionLabel: UILabel!
    
    var tappButtonItemHandler : ItemButtomHandler?
    var tapHandler : TapHandler?
    
    // MARK: - UIView Life Cycle
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        configureShadow()
        configureGesture()
    }

    // MARK: - Configuration methods
    
    func configure<T : ImageDownloadable & EndImageDownloadable & Titlable & Descriptionable>(delegate : T) {
        
        delegate.downloadImageInto(imageView: iconImageView)
        
        delegate.descriptionWith { [weak self](description) in
            self?.descriptionLabel.text = description
        }
        
        delegate.titleWith { [weak self](title) in
            self?.nameTitleLabel.text = title
        }
    }
    
    private func configureGesture() {
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cellTapped(_:))))
    }
    
    // MARK: - Handlers
    
    @objc private func cellTapped(_ sender : UITapGestureRecognizer) {
        tapHandler?(self)
    }
    
    @IBAction private func buttonTapped(_ sender: UIButton) {
        tappButtonItemHandler?(sender.tag)
    }
}
