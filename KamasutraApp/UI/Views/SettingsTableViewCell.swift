//
//  SettingsTableViewCell.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/21/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

final class SettingsTableViewCell: BaseSettingsTableViewCell {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var timeLabel: UILabel!
    
    // MARK : - UIView Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK : - Configuration methods
    
    override func configure<T : Settingsable>(delegate: T) {
        titleLabel.text = delegate.settingsTitle
        timeLabel.isHidden = !delegate.notShowTimeLabel
    }
}
