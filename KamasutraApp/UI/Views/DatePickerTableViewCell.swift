//
//  DatePickerTableViewCell.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/21/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit
import Material

final class DatePickerTableViewCell: TableViewCell {
    
    @IBOutlet private weak var datePicker: UIDatePicker!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
