//
//  CatalogItemCollectionViewCell.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 1/31/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit
import Material

final class CatalogCollectionItemView: CollectionViewCell, Shadowable {
    
    //UI
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak private var poseNameLabel: UILabel!
    @IBOutlet weak private  var descriptionButton: UIButton!
    
    @IBOutlet private weak var arrowImageView: UIImageView!
    @IBOutlet private weak var trayView: UIView!
    @IBOutlet private weak var descriptionPanelView: UIView!
    
    private var trayOriginalCenter: CGPoint!
    private var trayDownOffset: CGFloat!
    private var trayUp: CGPoint!
    private var trayDown: CGPoint!
    
    // MARK : - Life Cycle of UIView
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        configureShadow()
        configurePanGesture()
        cofigureTray()
        initialTrayConfiguration()
    }
    
    // MARK : - Configuration methods
    
    private func configurePanGesture() {
        let panGestureReconizer = UIPanGestureRecognizer(target: self, action: #selector(panGestureHandler(_:)))
        trayView.addGestureRecognizer(panGestureReconizer)
    }
    
    private func cofigureTray() {
        trayDownOffset = trayView.frame.bounds.height - 40
        trayUp = trayView.center
        trayDown = CGPoint(x: trayView.center.x ,y: trayView.center.y + trayDownOffset)
    }
    
    private func initialTrayConfiguration() {
        trayView.center = trayDown
        descriptionPanelView.isHidden = true
    }
    
    // MARK : - Handlers
    @objc private func panGestureHandler(_ sender : UIPanGestureRecognizer) {
        switch sender.state {
        case .began:
            trayOriginalCenter = trayView.center
        case .changed:
            let translation = sender.translation(in: trayView)
            if trayOriginalCenter.y + translation.y > trayView.frame.height { break }
            trayView.center = CGPoint(x: trayOriginalCenter.x, y: trayOriginalCenter.y + translation.y)
        case .ended:
            let velocity = sender.velocity(in: trayView)
            if velocity.y > 0 {
                UIView.animate(withDuration: 0.3) {[weak self] in
                    guard let taryView = self?.trayView, let trayDown = self?.trayDown else { return }
                    taryView.center = trayDown
                    self?.arrowImageView.image = #imageLiteral(resourceName: "ic_arrow_down")
                    self?.descriptionPanelView.isHidden = true
                }
            } else {
                UIView.animate(withDuration: 0.3) { [weak self] in
                    guard let taryView = self?.trayView, let trayUp = self?.trayUp else { return }
                    taryView.center = trayUp
                    self?.arrowImageView.image = #imageLiteral(resourceName: "ic_arrow_up")
                    self?.descriptionPanelView.isHidden = false
                }
            }
        default:
            break
        }
    }
}


