//
//  SelectCategoryHeaderView.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 1/29/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit
import Material

final class SelectCategoryHeaderView: UITableViewHeaderFooterView {
    
    typealias SectionTapHandler = (SectionType?) -> ()
    
    @IBOutlet weak private var categoriesLabel: UILabel!
    @IBOutlet weak private var allButton: Button!
    
    private var sectionType : SectionType?
    
    var sectionHandler : SectionTapHandler?
    
    // MARK : - Configuration methods
    
    func configure<T : Categorical & SectionTypable>(delegate : T) {
        categoriesLabel.text = delegate.sectionTitle
        sectionType = delegate.sectionType
    }
    
    // MARK : - Action Handler
    @IBAction private func allButtonTapped(_ sender: Button) {
        sectionHandler?(sectionType)
    }
}
