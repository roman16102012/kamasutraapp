//
//  ProfileDescriptionViewCell.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/20/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit
import Material

class ProfileDescriptionViewCell: TableViewCell {
    
    @IBOutlet weak private var containerView : UIView!
    
    // MARK : - Life Cycle of UIView
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureProfileView()
    }
    
    // MARK : - Configuration methods
    
    private func configureProfileView() {
        let view = ProfileView.instanceFromNib() ?? ProfileView()
        view.frame = containerView.bounds
        containerView.addSubview(view)
    }
}
