//
//  CategoryItemCollectionViewCell.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/2/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit
import Material

final class CategoryItemCollectionViewCell: CollectionViewCell, Shadowable {
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak private var iconImageView: UIImageView!
    @IBOutlet weak private var categoryNameLabel: UILabel!
    @IBOutlet weak private var categoryNumberLabel: UILabel!
    
    // MARK : - Life Cycle of UIView
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        configureShadow()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK : - Configuration methods
    
//    func configure<T :>() {
//
//    }
}
