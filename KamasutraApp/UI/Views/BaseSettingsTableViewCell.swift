//
//  BaseSettingsTableViewCell.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/21/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit
import Material

class BaseSettingsTableViewCell: TableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure<T : Settingsable>(delegate : T) {}
}
