//
//  HomeCollectionViewCell.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 1/29/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit
import Material

class HomeCollectionViewCell: CollectionViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = UIColor.clear
    }
//    func configure<T : >() {
//        //Noting to do
//    }
}
