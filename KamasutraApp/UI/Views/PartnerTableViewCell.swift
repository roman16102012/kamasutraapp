//
//  PartnerTableViewCell.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/9/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit
import Material

final class PartnerTableViewCell: TableViewCell {

    @IBOutlet weak private var iconImageView: UIImageView!
    @IBOutlet weak private var nameLabel: UILabel!
    @IBOutlet weak private var emailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
//    func configure<T : >(delegate : T) {
//
//    }
}
