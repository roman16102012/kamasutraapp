//
//  GamesCollectionViewCell.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 1/29/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

class GamesCollectionViewCell: HomeCollectionViewCell, Shadowable {
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak private var iconImageView: UIImageView!
    @IBOutlet weak private var gameNameLabel: UILabel!
    
    // MARK : - Life Cycle of UIView
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        configureShadow()
    }
}
