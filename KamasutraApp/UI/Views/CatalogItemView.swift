//
//  CatalogItemCollectionViewCell.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 1/31/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

final class CatalogCollectionItemView: UICollectionViewCell, Shadowable {
    
    //UI
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak private var poseNameLabel: UILabel!
    @IBOutlet weak private  var descriptionButton: UIButton!
    
    var descriptionPanelView: DescriptionPanelView?
    var previousTouchPoint : CGPoint?
    
    var viewDragging = false
    var viewPinned = false
    
    // MARK : - Life Cycle of UIView
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        configureShadow()
        let offset : CGFloat = 20
//        descriptionPanelView = configuredDescriptionPanel(offset) as? DescriptionPanelView
    }
    
    // MARK : - Configuration methods
    
    private func configuredDescriptionPanel(_ offset : CGFloat) -> UIView?{
        let frame = containerView.bounds.offsetBy(dx: 0, dy: containerView.bounds.height - offset)
        let view = DescriptionPanelView.instanceFromNib() ?? DescriptionPanelView()
        view.frame = frame
        containerView.addSubview(view)
//        configureGestureRecognizer(view: view)
//        configureBehavior(view: view)
        return view
    }
    
//    private func configureGestureRecognizer(view : UIView) {
//        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlerPanGestureRcognizer(sender:)))
//        view.addGestureRecognizer(panGestureRecognizer)
//    }
    
//    private func configureBehavior(view : UIView) {
//        let collision = UICollisionBehavior(items: [view])
//        collision.collisionDelegate = self
//        animator?.addBehavior(collision)
//
//        let boundary = view.frame.origin.y + view.frame.size.height
//
//        // lower boundary
//        var boundaryStart = CGPoint(x: 0, y: boundary)
//        var boudaryEnd = CGPoint(x: containerView.bounds.size.width, y: boundary)
//        collision.addBoundary(withIdentifier: 1 as NSCopying, from: boundaryStart, to: boudaryEnd)
//
//        // upper boundary
//        boundaryStart = CGPoint(x: 0, y: 0)
//        boudaryEnd = CGPoint(x : containerView.bounds.size.width, y : 0)
//        collision.addBoundary(withIdentifier: 2 as NSCopying, from: boundaryStart, to: boudaryEnd)
//
//        gravity?.addItem(view)
//
//        let itemBehavior = UIDynamicItemBehavior(items: [view])
//        animator?.addBehavior(itemBehavior)
//    }
    
    // MARK : - Handler
    
//    @objc private func handlerPanGestureRcognizer(sender : UIPanGestureRecognizer) {
//        guard let draggedView = sender.view else { return }
//        let touchPoint = sender.location(in: self)
//
//        switch sender.state {
//        case .began:
//            let dragStartPoint = sender.location(in: draggedView)
//            if dragStartPoint.y < 200 {
//                viewDragging = true
//                previousTouchPoint = touchPoint
//            }
//        case .changed:
//            let yOffset = (previousTouchPoint?.y ?? 0) - touchPoint.y
//            draggedView.center = CGPoint(x: draggedView.center.x , y: draggedView.center.y - yOffset)
//            previousTouchPoint = touchPoint
//        case .ended:
//            pin(view: draggedView)
//            addVelocity(draggedView, sender: sender)
//            animator?.updateItem(usingCurrentState: draggedView)
//            viewDragging = false
//        default: break
//        }
//    }
    
    //        if sender.state == .began {
    //            let dragStartPoint = sender.location(in: draggedView)
    //            if dragStartPoint.y < 200 {
    //                viewDragging = true
    //                previousTouchPoint = touchPoint
    //            }
    //        } else if sender.state == .changed && viewDragging {
    //            let yOffset = (previousTouchPoint?.y)! - touchPoint.y
    //            draggedView?.center = CGPoint(x: (draggedView?.center.x)!, y: (draggedView?.center.y)! - yOffset)
    //            previousTouchPoint = touchPoint
    //        } else if sender.state == .ended && viewDragging {
    //
    //            // pin
    //            //addVelocity
    //            pin(view: draggedView!)
    //            addVelocity(draggedView!, sender: sender)
    //            animator?.updateItem(usingCurrentState: draggedView!)
    //            viewDragging = false
    //        }
    
//    func pin(view : UIView) {
//        let viewHasRichedPinLocation = view.frame.origin.y < 100
//        if viewHasRichedPinLocation {
//            if !viewPinned {
//                let snapPosition = self.center
//                snap = UISnapBehavior(item: view, snapTo: snapPosition)
//                animator?.addBehavior(snap!)
//
//                viewPinned = true
//            }
//        } else {
//            if viewPinned {
//                animator?.removeBehavior(snap!)
//                viewPinned = false
//            }
//        }
//    }
    
//    func addVelocity(_ view : UIView, sender : UIPanGestureRecognizer) {
//        var velocity = sender.velocity(in: self)
//        velocity.x = 0
//        if let behaviour = itemBehavior(view) {
//            behaviour.addLinearVelocity(velocity, for: view)
//        }
//    }
    
//    func itemBehavior(_ view : UIView) -> UIDynamicItemBehavior? {
//        for behavior in (animator?.behaviors)! {
//            if let itemBehavour = behavior as? UIDynamicItemBehavior {
//                if let possibleView = itemBehavour.items.first as? UIView, possibleView == view {
//                    return itemBehavour
//                }
//            }
//        }
//        return nil
//    }
}

//extension CatalogItemView : UICollisionBehaviorDelegate {
//    func collisionBehavior(_ behavior: UICollisionBehavior, beganContactFor item: UIDynamicItem, withBoundaryIdentifier identifier: NSCopying?, at p: CGPoint) {
//        let view = item as! UIView
//        pin(view: view)
//    }
//}

