//
//  MyRewardCollectionViewCell.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/7/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

class MyRewardCollectionViewCell: HomeCollectionViewCell, Shadowable {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak private var rewardNameLabel: UILabel!
    
    // MARK: - UIView life cycle
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        configureShadow()
    }
}
