//
//  CategoryCollectionViewCell.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 1/29/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: HomeCollectionViewCell, Shadowable {

    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet private weak var iconImageView: UIImageView!
    @IBOutlet private weak var numberLabel: UILabel!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        configureShadow()
    }
}
