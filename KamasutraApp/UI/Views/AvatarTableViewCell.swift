//
//  AvatarTableViewCell.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/20/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit
import Material

class AvatarTableViewCell: TableViewCell {
    
    @IBOutlet weak var avatarImageView : UIImageView!
    @IBOutlet weak var userNameLabel : UILabel!
    @IBOutlet private weak var changePhotoLabel : UILabel!
    
    var isEdited : Bool = false {
        didSet {
            changePhotoLabel.isHidden = !isEdited
        }
    }
    
     // MARK : - Life Cycle of UIView
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureChangeLabel()
        // Initialization code
    }
    
    // MARK : - Configuration methods
    
    private func configureChangeLabel() {
        changePhotoLabel.isHidden = true
    }
}
