//
//  Credentials.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/14/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import ObjectMapper

struct SignInCredentials : Mappable {
    
    var email : String?
    var password : String?
    
    //MARK : - Mappable
    
    init?(map: Map) {
        guard map.JSON["email"] != nil, map.JSON["password"] != nil else { return nil }
    }
    
    mutating func mapping(map: Map) {
        email <- map["email"]
        password <- map["password"]
    }
    
    // MARK : - Initialization methods
    
    init?(dict : [String : Any]) {
        guard let email = dict["email"] else { return nil }
        guard let password = dict["password"] else { return nil }
        self.email = email as? String
        self.password = password as? String
    }
}
