//
//  Category.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/13/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import ObjectMapper

struct Category : Mappable {
    
    var id : Int?
    var name : String?
    var count_of_posts : Int?
    
    //MARK : - Mappable
    
    init?(map: Map) {
        guard map.JSON["id"] != nil else { return nil }
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        count_of_posts <- map["count_of_posts"]
    }
}
