//
//  Pose.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/14/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import ObjectMapper

struct Pose : Mappable {
    
    var id : Int?
    var name : String?
    var imageURLStrings : [String]?
    var categories : [Int]?
    var description : String?
    
    //MARK : - Mappable
    
    init?(map: Map) {
        guard map.JSON["id"] != nil else { return nil }
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        imageURLStrings <- map["images"]
        description <- map["description"]
    }
}
