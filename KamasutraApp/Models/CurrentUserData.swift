//
//  CurrentUserData.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/14/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import ObjectMapper

final class CurrentUserData : NSObject, Mappable, NSCoding {
    
    var currentUser : CurrentUser?
    var auth_token : String?
    
    //MARK : - Mappable
    
    required init?(map: Map) {
        guard map.JSON["user"] != nil else { return nil }
        guard map.JSON["auth_token"] != nil else { return nil }
    }
    
    func mapping(map: Map) {
        currentUser <- map["user"]
        auth_token <- map["auth_token"]
    }
    
    // MARK: - NSCoding
    
    init(currentUser: CurrentUser, auth_token: String) {
        self.currentUser = currentUser
        self.auth_token = auth_token
    }
    
    required convenience init?(coder decoder: NSCoder) {
        guard let currentUser = decoder.decodeObject(forKey: "currentUser") as? CurrentUser,
            let auth_token = decoder.decodeObject(forKey: "auth_token") as? String
            else { return nil }
        self.init(currentUser: currentUser, auth_token: auth_token)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(currentUser, forKey: "currentUser")
        aCoder.encode(auth_token, forKey: "auth_token")
    }
}
