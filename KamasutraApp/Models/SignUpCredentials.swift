//
//  SignUpCredentials.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/15/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import ObjectMapper

struct SignUpCredentials : Mappable {
    
    var email : String?
    var password : String?
    var name : String?
    var user_name : String?
    
    //MARK : - Mappable
    
    init?(map: Map) {
        guard map.JSON["email"] != nil, map.JSON["password"] != nil, map.JSON["name"] != nil, map.JSON["user_name"] != nil else { return nil }
    }
    
    mutating func mapping(map: Map) {
        email <- map["email"]
        password <- map["password"]
        name <- map["name"]
        user_name <- map["user_name"]
    }
    
    // MARK : - Initialization methods
    
    init?(dict : [String : Any]) {
        guard let email = dict["email"], let password = dict["password"], let name = dict["name"], let user_name = dict["user_name"] else { return nil }
        self.email = email as? String
        self.password = password as? String
        self.name = name as? String
        self.user_name = user_name as? String
    }
}
