//
//  Status.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/15/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import ObjectMapper

enum StatusType : String {
    case success = "success"
    case failure = "failure"
}

struct Status : Mappable {
    
    var error : Any?
    var status : StatusType?
    
    //MARK : - Mappable
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        error <- map["error"]
        status <- map["status"]
    }
}
