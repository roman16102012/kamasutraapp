//
//  User.swift
//  MVP
//
//  Created by Roman S Vovk on 11/15/17.
//  Copyright © 2017 Roman S Vovk. All rights reserved.
//
import Foundation
import AlamofireObjectMapper
import ObjectMapper

struct CurrentUser: Mappable{
    
    var id : Int?
    var email : String?
    var name : String?
    var user_name : String?
    
    //MARK : - Mappable
    
    init?(map: Map) {
        guard map.JSON["id"] != nil else { return nil }
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        email <- map["email"]
        name <- map["name"]
        user_name <- map["user_name"]
    }
}
