//
//  ItemViewModel.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 1/26/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

class ItemViewModel : ImageDownloadable, EndImageDownloadable, Titlable, Descriptionable {

    func downloadImageInto(imageView : UIImageView) {
        DispatchQueue.main.async {/*[weak self, weak model = self.model] in*/
            imageView.af_setImage(withURL: URL(string: "www.google.com")!, placeholderImage: #imageLiteral(resourceName: "ic_placeholder"))
        }
    }
    
    func titleWith(textHandler: OptionalStringHandler?) {
        
    }
    
    func descriptionWith(textHandler: OptionalStringHandler?) {
        
    }
}
