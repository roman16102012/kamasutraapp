//
//  SectionViewModel.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 1/29/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

struct SectionViewModel : Categorical, Multiplyerable, SectionTypable, CellIdentifiable {
    
    var sectionType: SectionType
    
    var sectionTitle : String? {
        return "\(sectionType)"
    }
    
    var cellIdentifier : String? {
        switch sectionType {
        case .catalogType, .listType:
            return String(describing: CatalogCollectionViewCell.self)
        case .categoriesType:
            return String(describing: CategoryCollectionViewCell.self)
        case .gamesType:
            return String(describing: GamesCollectionViewCell.self)
        case .myRewardsType:
            return String(describing: MyRewardCollectionViewCell.self)
        }
    }
    
    var multiplyer: CGFloat {
        return (sectionType == .gamesType) ? 1.3 : 2.5
    }
    
    init?(index : Int) {
        guard let sectionType = SectionType(rawValue: index) else { return nil }
        self.sectionType = sectionType
    }
}
