//
//  DetailsViewModel.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/6/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

struct DetailsViewModel : Categorical, Multiplyerable, CellIdentifiable {
    
    private var sectionType: SectionType
    
    init?(sectionType : SectionType?) {
        guard let sectionType = sectionType else {return nil}
        self.sectionType = sectionType
    }
    
    var sectionTitle : String? {
        switch sectionType {
        case .gamesType:
            return "Games"
        case .categoriesType:
            return "Categoties"
        default:
            return nil
        }
    }
    
    var cellIdentifier : String? {
        switch sectionType {
        case .categoriesType:
            return String(describing: CategoryItemCollectionViewCell.self)
        case .gamesType:
            return String(describing: GamesCollectionViewCell.self)
        default:
            return nil
        }
    }
    
    var multiplyer : CGFloat  { // Multiplier is used to define size of cell item on GamesOrCategoriesViewController
        switch sectionType {
        case .categoriesType:
            return 5.0
        case .gamesType:
            return 2.0
        default:
            return 1.0
        }
    }
    
    init(sectionType : SectionType) {
        self.sectionType = sectionType
    }
}
