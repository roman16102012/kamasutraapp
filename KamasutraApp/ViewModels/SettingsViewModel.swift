//
//  SettingsViewModel.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/21/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit

private enum SettingsType : Int {
    case changePassword = 0
    case playTime
    case logout
}

struct SettingsViewModel : CellIdentifiable, Settingsable {
    
    private var type : SettingsType
    
    var cellIdentifier: String? {
        switch type {
        case .changePassword, .playTime:
            return String(describing: SettingsTableViewCell.self)
        case .logout:
            return String(describing: LogOutTableViewCell.self)
        }
    }
    
    var notShowTimeLabel: Bool {
        return type == .playTime
    }
    
    var settingsTitle : String {
        switch type {
        case .changePassword:
            return "Change Password"
        case .playTime:
            return "Player Time"
        case .logout:
            return "Logout"
        }
    }
    
    init?(index : Int) {
        guard let type = SettingsType(rawValue: index) else { return nil }
        self.type = type
    }
}
