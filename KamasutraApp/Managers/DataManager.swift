//
//  DataManager.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/13/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

enum Hosts: String {
    case Development = "192.168.50.75:5000"
}

enum DataError: Error {
    case NotAuthenticated
}

enum DataStatusCode: Int {
    case OK = 200
    case Unauthorized = 401
    case NotFound = 404
}

typealias DataResult<T> = Alamofire.Result<T>

final class DataManager {
    
    static var activeHost: Hosts {
        return .Development
    }
    
    static func serverUrl() throws -> URL  {
        return try activeHost.rawValue.asURL()
    }
    
    // Generate auth_token
    
    // handler is invoked on the Main queue
    //
    static func generateToken<T : Mappable>(email: String, password : String, handler : ((DataResult<T>, _ statusCode: Int) -> ())?) {
        guard let credentials = SignInCredentials(dict: ["email" : email, "password" : password]) else { return }
        validatePerformingSimilarRequest(to: AuthRoute.generate_token(credentials: credentials)) {
            Alamofire.request(AuthRoute.generate_token(credentials: credentials))
                .responseObject(queue: DispatchQueue.global()) { (response : DataResponse<T>) in
                    DispatchQueue.main.async {
                        handler?(response.result, response.response?.statusCode ?? DataStatusCode.OK.rawValue)
                    }
            }
        }
    }
    
    static func signUp<T : Mappable>(email : String, password : String, name : String, user_name : String, handler : ((DataResult<T>, _ statusCode: Int) -> ())?) {
        guard let credentials = SignUpCredentials(dict: ["email" : email, "password" : password, "name" : name, "user_name" : user_name]) else { return }
        validatePerformingSimilarRequest(to: AuthRoute.signup(credentials: credentials)) {
            Alamofire.request(AuthRoute.signup(credentials: credentials))
                .responseObject(queue: DispatchQueue.global()){ (response : DataResponse<T>) in
                    DispatchQueue.main.async {
                        handler?(response.result, response.response?.statusCode ?? DataStatusCode.OK.rawValue)
                    }
            }
        }
    }
    
    // Get categories
    
    static func allCategories<T : Mappable>(handler : ((DataResult<[T]>, _ statusCode: Int) -> ())?) {
        Alamofire.request(CategoriesRoute.allCategories)
            .responseArray(queue: DispatchQueue.global(), keyPath: CategoriesRoute.arrayKeyPath) { (response: DataResponse<[T]>) in
                DispatchQueue.main.async {
                    handler?(response.result, response.response?.statusCode ?? DataStatusCode.OK.rawValue)
                }
        }
    }
    
    // Get Poses
    
    static func allPose<T : Mappable>(handler : ((DataResult<[T]>, _ statusCode: Int) -> ())?) {
        Alamofire.request(PoseRoute.allPoses)
            .responseArray(queue: DispatchQueue.global(), keyPath: PoseRoute.arrayKeyPath) { (response: DataResponse<[T]>) in
                DispatchQueue.main.async {
                    handler?(response.result, response.response?.statusCode ?? DataStatusCode.OK.rawValue)
                }
        }
    }
    
    static func posesByCategoryId<T : Mappable>(id : Int, handler : ((DataResult<[T]>, _ statusCode: Int) -> ())?) {
        Alamofire.request(PoseRoute.posesByCategoryId(categoryId: id))
            .responseArray(queue: DispatchQueue.global(), keyPath: PoseRoute.arrayKeyPath) { (response: DataResponse<[T]>) in
                DispatchQueue.main.async {
                    handler?(response.result, response.response?.statusCode ?? DataStatusCode.OK.rawValue)
                }
        }
    }
    
    static func poseById<T : Mappable>(id : Int, handler : ((DataResult<T>, _ statusCode: Int) -> ())?) {
        Alamofire.request(PoseRoute.poseById(id: id))
            .responseObject(queue: DispatchQueue.global()) { (response: DataResponse<T>) in
                DispatchQueue.main.async {
                    handler?(response.result, response.response?.statusCode ?? DataStatusCode.OK.rawValue)
                }
        }
    }
    
    // MARK : - Additinal methods
    
//    static func userAuthenticationHeaders() -> [String : String]? {
//        
//    }
    
    private static func validatePerformingSimilarRequest(to urlRequest: URLRequestConvertible, completion: @escaping () -> ()) {
        Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
            var present = false
            let request = urlRequest.urlRequest
            for task in tasks {
                if task.originalRequest == request {
                    print("Cancelled reqest: \(String(describing: request))")
                    present = true
                    break
                }
            }
            if !present { completion() }
        }
    }
}
