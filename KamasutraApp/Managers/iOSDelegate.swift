//
//  iOSDelegate.swift
//  MVP
//
//  Created by Roman S Vovk on 11/10/17.
//  Copyright © 2017 Roman S Vovk. All rights reserved.
//

import Foundation

enum iOSEventType {
    case iOSEventAuthorized
    case iOSEventLogined
    case iOSEventNotAuthorized
    case iOSEventRemoteSettingsUpdate
    case iOSEventDataUpdate
    case iOSEventNeedToConnect
    case iOSEventUnautorized
}

struct SQiOSEvent {
    let type : iOSEventType
}

protocol iOSDelegate : class {
    func handleEvent(type : iOSEventType)
}
