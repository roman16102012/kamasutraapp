//
//  CacheManager.swift
//  MVP
//
//  Created by Roman S Vovk on 11/15/17.
//  Copyright © 2017 Roman S Vovk. All rights reserved.
//

import Foundation

final class CacheManager {
    
    static var currentUserData : CurrentUserData? {
        get {
            guard let data = UserDefaults.standard.value(forKey: "CurrentUserData") as? Data else { return nil }
            return NSKeyedUnarchiver.unarchiveObject(with: data) as? CurrentUserData
        }
        set {
            var user: Data?
            if let newUser = newValue {
                user = NSKeyedArchiver.archivedData(withRootObject: newUser)
            }
            UserDefaults.standard.set(user, forKey: "CurrentUserData")
            UserDefaults.standard.synchronize()
        }
    }
    
    static func cleanAll() {
        currentUserData = nil
    }
}
