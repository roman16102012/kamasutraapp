//
//  iOS.swift
//  MVP
//
//  Created by Roman S Vovk on 11/10/17.
//  Copyright © 2017 Roman S Vovk. All rights reserved.
//

import UIKit

final class iOS {
    
    weak var delegate : iOSDelegate?
    
    static var shared : iOS = {
        let sharediOS = iOS()
        return sharediOS
    }()
    
    // MARK : - Managers
    
    var applicationController : ApplicationController = {
        return ApplicationController()
    }()
    
    //MARK: - Autorization methods
    
    func autorize(delegate : iOSDelegate?) {
        guard delegate != nil && self.delegate != nil else {
            return;
        }
        self.delegate = delegate
    }
    
    func autorize() {
        autorize(delegate: nil)
    }
    
    func configureWith(window : UIWindow?) {
        applicationController.defineScenarioWith(window: window)
    }
    
    //MARK: - Initialization methods
    private init() {
    //Nothing to do
    }
}
