import Foundation
import Alamofire

enum RouteError: Error {
    case invalidURL
}

protocol RouteComponents {
    var authenticationHeaders: [String: String]?  { get }
    var headers: [String: String]?  { get }
    var method: HTTPMethod { get }
    var body: Data? { get }
    var path: String { get }
    var query: String  { get }
    var parameters: [String: AnyObject]? { get }
}

extension RouteComponents {
    var authenticationHeaders: [String: String]?  { return nil }
    var headers: [String: String]? { return ["Content-Type" : "application/json"] }
    var method: HTTPMethod { return .get }
    var body: Data? { return nil }
    var path: String { return "" }
    var query: String  { return "" }
    var parameters: [String: AnyObject]? { return nil }
}

protocol Route: URLRequestConvertible, RouteComponents { }

extension Route {
    
    func asURLRequest() throws -> URLRequest {
        let url = try DataManager.serverUrl()
        
        var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)
        urlComponents?.path = url.path + path
        urlComponents?.query = query
        
        guard let fullUrl = urlComponents?.url else {
            throw RouteError.invalidURL
        }
        
        var urlRequest = URLRequest(url: fullUrl)
        urlRequest.httpMethod = method.rawValue
        urlRequest.httpBody = body
        urlRequest.addHeaders(headers: authenticationHeaders ?? ["":""])//SessionManager.userAuthenticationHeaders())
        urlRequest.addHeaders(headers: headers)
        urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        
        return urlRequest
    }
}

extension URLRequest {
    
    mutating func addHeaders(headers: [String: String]?) {
        if let headers = headers {
            for (key, value) in headers {
                self.addValue(value, forHTTPHeaderField: key)
            }
        }
    }
}

