//
//  Pose.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/14/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import Foundation
import Alamofire

enum PoseRoute : Route {
    
    static let arrayKeyPath = "posts"
    
    case allPoses
    case poseById(id : Int)
    case posesByCategoryId(categoryId : Int)
    
    var method: HTTPMethod {
        return .get
    }
    
    var path: String {
        switch self {
        case .allPoses:
            return "/posts"
        case .poseById(let id):
            return "/post/\(id)"
        case .posesByCategoryId(let categoryId):
            return "/posts_by_category/\(categoryId)"
        }
    }
}
