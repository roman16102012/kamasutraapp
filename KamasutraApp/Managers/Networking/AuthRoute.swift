//
//  UserRoute.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/13/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import Foundation
import Alamofire

enum AuthRoute : Route {
    
    case generate_token(credentials : SignInCredentials)
    case signup(credentials : SignUpCredentials)
    case reset_password
    case verify_token
    case logout
    
    var method: HTTPMethod {
        return .post
    }
    
    var path: String {
        switch self {
        case .generate_token:
            return "/auth/generate-token"
        case .signup:
            return "/auth/signup"
        case .logout:
            return "/auth/logout"
        case .verify_token:
            return "/auth/verify-token"
        case .reset_password:
            return "/auth/reset_password"
        }
    }
    
    var body: Data? {
        switch self {
        case .generate_token(let credentials):
            return credentials.toJSONString()?.data(using: .utf8)
        case .signup(let credentials):
            return credentials.toJSONString()?.data(using: .utf8)
        default:
            return nil
        }
    }
}
