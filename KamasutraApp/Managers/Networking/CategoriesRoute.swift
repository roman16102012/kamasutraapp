//
//  CategoriesRoute.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 2/13/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import Foundation
import Alamofire

enum CategoriesRoute : Route {
    
    static let arrayKeyPath = "categories"
    
    case allCategories
    
    var method : HTTPMethod {
        switch self {
        case .allCategories:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .allCategories:
            return "/categories"
        }
    }
}
