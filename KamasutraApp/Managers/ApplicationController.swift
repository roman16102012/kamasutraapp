//
//  ApplicationController.swift
//  KamasutraApp
//
//  Created by Roman S Vovk on 1/24/18.
//  Copyright © 2018 Roman S Vovk. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

enum State {
    case Initial
    case Authenticated
    case Invalid
}

enum StoryboardType : String, CustomStringConvertible {
    case main
    case login
    
    var description: String {
        switch self {
        case .main:
            return "Main"
        case .login:
            return "Login"
        }
    }
}

struct StateMachine {
    
    private var state: State = .Initial
    
    var currentState: State {
        return state
    }
    
    mutating func updateState(state: State) {
        self.state = state
    }
}

final class ApplicationController: NSObject, Rootable {
    
    weak var window : UIWindow?
    
    var stateMachine: StateMachine = StateMachine()
    
    fileprivate var mainStoryBoard = UIStoryboard(name: "\(StoryboardType.main)", bundle: nil)
    fileprivate var loginStoryBoard = UIStoryboard(name: "\(StoryboardType.login)", bundle: nil)
    
    lazy var rootViewController : TabBarController? = {[weak self] in
        let rootViewController = self?.mainStoryBoard.instantiateInitialViewController() as? TabBarController
        return rootViewController
        }()
    
    var rootNavigationViewController : UINavigationController? {
        return rootViewController?.rootNavigationViewController
    }
    
    lazy var login : LoginNavigationController? = {[weak self] in
        return self?.loginStoryBoard.instantiateInitialViewController() as? LoginNavigationController
        }()
    
    lazy var sideMenu : SSASideMenu = {[weak self] in
        let contentViewController = self?.mainStoryBoard.instantiateViewController(withIdentifier: String(describing: MenuViewController.self)) as? MenuViewController
        let sideMenu = SSASideMenu(contentViewController: rootViewController!, leftMenuViewController: contentViewController!)
        sideMenu.configure(SSASideMenu.MenuViewEffect(fade: true, scale: true, scaleBackground: false))
        sideMenu.configure(SSASideMenu.ContentViewEffect(alpha: 1.0, scale: 0.7))
        sideMenu.configure(SSASideMenu.ContentViewShadow(enabled: true,
                                                         color: UIColor.black,
                                                         offset: CGSize(width: -30, height: -30),
                                                         opacity: 0.5,
                                                         radius: 10.0))
        sideMenu.delegate = self
        return sideMenu
        }()
    
    private func configureAutorizedScenarioWith() {
        window?.rootViewController = sideMenu
    }
    
    private func configureNotAutorizedScenarioWith() {
        window?.rootViewController = login
    }
    
    func defineScenarioWith(window : UIWindow?, animated : Bool = true) {
        self.window = window
        switch stateMachine.currentState {
        case .Initial:
            configureAutorizedScenarioWith()
        case .Authenticated:
            configureNotAutorizedScenarioWith()
        default:
            break
        }
    }
    
    // MARK : - Notautorized Scenario methods
    
    func presentForgotPassword(is animated : Bool = true) {
        if let forgotPasswordViewControlller = loginStoryBoard.instantiateViewController(withIdentifier: String(describing: ForgotPasswordViewController.self)) as? ForgotPasswordViewController {
            login?.pushViewController(forgotPasswordViewControlller, animated: animated)
        }
    }
    
    // MARK : - Autorized Scenarios methdos
    
    func presentHomeScreenDetails(sectionType : SectionType) {
        switch sectionType {
        case .catalogType:
            presentCatalogOn()
        case .listType:
            presentListsOn()
        case .categoriesType, .gamesType:
            presentGamesOrCategoriesOn(sectionType: sectionType)
        default:
            break
        }
    }
    
    func presentCategoryOn(is animated : Bool = true) {
        if let homeDetailsViewController = mainStoryBoard.instantiateViewController(withIdentifier: String(describing: CategoryViewController.self)) as? CategoryViewController {
            rootNavigationViewController?.pushViewController(homeDetailsViewController, animated: animated)
        }
    }
    
    func presentCatalogOn(is animated : Bool = true) {
        if let catlogViewController = mainStoryBoard.instantiateViewController(withIdentifier: String(describing: CatalogViewController.self)) as? CatalogViewController {
            rootNavigationViewController?.pushViewController(catlogViewController, animated: animated)
        }
    }
    
    func presentListsOn(is animated : Bool = true) {
        if let listsViewController = mainStoryBoard.instantiateViewController(withIdentifier: String(describing: ListsViewController.self)) as? ListsViewController {
            rootNavigationViewController?.pushViewController(listsViewController, animated: animated)
        }
    }
    
    func presentGamesOrCategoriesOn(sectionType : SectionType, is animated : Bool = true) {
        if let gamesOrCategoriesViewController = mainStoryBoard.instantiateViewController(withIdentifier: String(describing: GamesOrCategoriesViewController.self)) as? GamesOrCategoriesViewController {
            gamesOrCategoriesViewController.configure(delegate: DetailsViewModel(sectionType: sectionType))
            rootNavigationViewController?.pushViewController(gamesOrCategoriesViewController, animated: animated)
        }
    }
    
    func presentSettings(is animated : Bool = true) {
        if let settingsViewController = mainStoryBoard.instantiateViewController(withIdentifier: String(describing: SettingsViewController.self)) as? SettingsViewController {
            rootNavigationViewController?.pushViewController(settingsViewController, animated: animated)
        }
    }
    
    func presentChangePassword(is animated : Bool = true) {
        if let changePasswordViewController = loginStoryBoard.instantiateViewController(withIdentifier: String(describing: ChangePasswordViewController.self)) as? ChangePasswordViewController {
            rootNavigationViewController?.pushViewController(changePasswordViewController, animated: animated)
        }
    }
    
    // MARK : - Initialization methods
    
    override init() {
        super.init()
        configureKeyboardManager()
    }
    
    // MARK : - Configuration methods
    
    private func configureKeyboardManager() {
        IQKeyboardManager.sharedManager().enable = true
    }
}

extension ApplicationController : SSASideMenuDelegate {
    
    func sideMenuDidRecognizePanGesture(_ sideMenu: SSASideMenu, recongnizer: UIPanGestureRecognizer) {
        //
    }
    
    func sideMenuWillShowMenuViewController(_ sideMenu: SSASideMenu, menuViewController: UIViewController) {
        
    }
    
    func sideMenuDidShowMenuViewController(_ sideMenu: SSASideMenu, menuViewController: UIViewController) {
        
    }
    
    func sideMenuWillHideMenuViewController(_ sideMenu: SSASideMenu, menuViewController: UIViewController) {
        
    }
    
    func sideMenuDidHideMenuViewController(_ sideMenu: SSASideMenu, menuViewController: UIViewController) {
        
    }
}
